// Copyright (C) 2024 Daniel Cerqueira

// This file is part of language-redirector.

// language-redirector is free software: you can redistribute it
// and/or modify it under the terms of the GNU Affero General Public
// License as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.

// language-redirector is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public
// License along with language-redirector. If not, see
// <https://www.gnu.org/licenses/>.


(function() {

    const mainWebsiteLanguage = 'pt'

    const redirected = sessionStorage.getItem('lang-redirected')

    if(!redirected) {

        const browserLang = navigator.language ? navigator.language : navigator.userLanguage;

        const separatorIndex = browserLang.indexOf('-')

        const browserLangFamily = (separatorIndex) ? browserLang.substr(0, separatorIndex) : browserLang

        // remove language prefix if main language of website ( it portuguese use / instead of /pt )
        const browserLangOutput = (browserLangFamily == mainWebsiteLanguage) ? '' : browserLangFamily

        const query = ( location.search.length ) ? ( '?' + location.search ) : location.search

        const targetHref = `${location.origin}/${browserLangOutput}${location.search}${location.hash}`

        sessionStorage.setItem('lang-redirected', 'true')

        window.open(targetHref, '_self')

    }

}());
